This program will simulate the playing of a Football World Cup. 
This is a simplification of the actual FIFA World Cup but the simulation develped required no prior knowledge of the rules of football.
Only a text interface is used for this program, there is to be no GUI. 

System description


The Football World Cup is played between 4 teams. The teams are ranked from 1 to 4, with 1 indicating the highest ranking and 4 the lowest.
The Cup is played in two stages: Preliminary and Final.
During the Preliminary stage, each team plays the other three teams resulting in a total of six (6) games played. 
The games during the Preliminary stage occur sequentially and the result of each game is shown on the screen as it is completed. 
In the Preliminary stage a team gains 3 points for a win, 1 point for a draw, and 0 points for a loss.
At the end of the Preliminary stage the system will display a summary of the results with the teams sorted from first to last, based on the following criteria:

• The teams will be listed from most to least points.
• If teams have the same points, then the team with the higher number of goals scored will be placed higher on the table.
• If teams cannot be separated by any of the previous criteria, then the higher placing in the table will be determined randomly.

After the Preliminary stage is completed, the top two teams play in the Final. 
The only difference between the final and the preliminary games is that the final must have a winner, the Football World Cup Champions!
At the end of the Final the overall results are displayed on the screen, including the name of the Football World Cup champion team, 
the Golden Boot Award player, and the Fair Play Award team.

Class Design

There are four classes: Player. Team, Game and Goals.

Player

An object of Player will have the following fields:

name – of type String. The name can contain only alphabetical characters and at most one hyphen, ‘-‘, to accommodate names that may be hyphenated 
such as Zeta-Jones. There must be a minimum of two alphabetic characters in the name and it cannot begin or end with a hyphen. 
The two players in a team cannot have the same name.

goals – of type int representing the number of goals scored. This is used to determine the ‘Golden Boot’ award for the top goal scorer.

Team

An object of Team has the following fields:

name – of type String and is the name of the country the team is representing.

ranking – of type int. This field refers to the team’s relative strength compared to the other teams in the Cup. 
No two teams can have the same ranking. The ranking must be between 1 and 4 inclusive (as there are only be four teams in the competition).

2 players – of type Player. Each team will have two players who are the goal scorers.

Each team will also have a yellow card and red card score. 
A yellow card is shown to players who have committed serious fouls, while a red card is shown to players who have committed more severe offences 
and they are sent from the field. 
The cards are associated with a team, not an individual player. 
The showing of yellow and red cards is determined randomly, both are rare and some games have no cards shown at all. 
But there are usually four times as many yellow cards shown as red cards. 
The total yellow and red card marks will determine a team’s Fair Play score, which will be used to determine the Cup’s Fair Play Award. 
Each yellow card is worth one mark,

Game

The Game class has an ArrayList of Team objects. The Game class also has methods: playGame(), playPenaltyShootOut(), play genCards(), goldenBoot(),
delay(),displayGameResult(), etc.
The playGame() method simulates the playing of a game between two teams. 
This is done by randomly generating the number of goals scored by each team. 
The number of goals generated is in a specified range. The team with the highest ranking will have a greater chance of winning. 
This is simulated by giving the higher ranked team a wider range of possible goals as follows:

• Higher ranked team: a goal range of 0 to (5 + a random upset (a random number between 0 and 2))
• Lower ranked team: a goal range of 0 to ((5 – difference in team rank) + a random upset (a random number between 0 and 2))
The goals are randomly distributed between the two players.
The playPenaltyShootOut() method simulates the playing of a penalty shoot-out, if required for a Final that ends in a draw. 
One player of each team has five shots at goal. The team whose player has the highest number of goals at the end of the five shots wins. 
If the score is equal then each player has another shot at goal. This continues until there is a result. 
The goals scored by a player in a penalty shoot-out are not counted towards the Golden Boot Award.

Goals

This class only has the random number generation function which helps in generating random values within a specific range
at multiple occassions through out the game simulation.

